/////////////////////////
//// Histograms demo ////
/////////////////////////
//#include "HepMCParser.hh"
#include "Hammer/Hammer.hh"
#include <memory>
#include <boost/thread.hpp>
#include <iostream>
#include <fstream>
#include <utility>
#include <string>
#include "TFile.h"
#include "TLatex.h"
#include "TString.h"
#include "TLegend.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "THn.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TVectorD.h"
#include "TCollection.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TFormula.h"
#include "TBranch.h"
#include "TTree.h"
#include "TInterpreter.h"
#include <cmath>
#include <math.h>
#include "omp.h"
#include <thread>
#include <future>

using namespace std;

static constexpr size_t NUM = 1;

inline void parallelFor(size_t numThreads, size_t start, size_t end, std::function<void(size_t start, size_t end)> work) {
  size_t workSize = (end - start)/ numThreads;

  std::vector<std::thread> my_threads(numThreads);

  for (size_t i = 0; i < numThreads; ++i) {
    size_t batchStart = start + i * workSize;
    my_threads[i] = std::thread(work, batchStart, batchStart + workSize);
  }
  size_t lastStart = start +  numThreads * workSize;
 
  std::for_each(my_threads.begin(), my_threads.end(), std::mem_fn(&std::thread::join));
}

//some calculation with the histogram contents
inline double calcHisto(Hammer::IOHistogram& histo) {
  double res = 0.0;
  for (size_t n_bin = 1; n_bin < 2401; ++n_bin)
    {
      res += histo[n_bin].sumWi;
    }
 
  return res;
}

typedef std::chrono::duration<float> float_seconds;

int main() {

  //////////////////////////////////////////////////
  //// Now reread histograms and play with them ////
  //////////////////////////////////////////////////

  Hammer::Hammer ham{};
  ham.initRun();
  TFile* inFile = new TFile("Hammer_file.root", "READ"); 
  if (!inFile) {
    throw std::runtime_error("RooHammerFuncERROR: HAMMER buffer file not found. Aborting!");
  }

  TTree* tree;
  inFile->GetObject("Hammer",tree);
  TBranch* brecord = nullptr;
  TBranch* btype = nullptr;
  Hammer::IOBuffer buf{Hammer::RecordType::UNDEFINED, 0ul, new uint8_t[1024*1024*1024]};
  tree->SetBranchAddress("record",buf.start, &brecord);
  tree->SetBranchAddress("type",&buf.kind, &btype);
  Long64_t nrecords = tree->GetEntries();
  auto entry = tree->LoadTree(0);
  brecord->GetEntry(entry);
  btype->GetEntry(entry);
  if(buf.kind!='b'){                                                                         
    delete tree;
    inFile->Close();
    delete inFile;
    delete[] buf.start;
    throw std::runtime_error("ERROR: HAMMER buffer not found. Aborting!");
  }

  for(Int_t i = 0; i<nrecords; ++i){
    entry = tree->LoadTree(i);
    brecord->GetEntry(entry);
    btype->GetEntry(entry);
    if(buf.kind == Hammer::RecordType::HEADER){
      ham.loadRunHeader(buf,kTRUE);
    }
    else if(buf.kind == Hammer::RecordType::HISTOGRAM_DEFINITION) {
      ham.loadHistogramDefinition(buf,kTRUE);
    }
    if( buf.kind == Hammer::RecordType::HISTOGRAM){
      ham.loadHistogram(buf);
    } 
  }
    tree->ResetBranchAddresses();
    delete tree;
    inFile->Close();
    delete inFile;
    delete[] buf.start;

    array<double, NUM> serialResults;
    array<double, NUM> parallelResults;


    auto fun = [](size_t id, Hammer::Hammer* pHam, array<double, NUM>* results) -> void {

    double val = (static_cast<double>(id)) * 0.001 - 10.;
    std::string histoname = "costheta_lrec_vs_costheta_drec_vs_chirec_vs_q2rec";
    // Set the WCs
    // "SM", "S_qLlL", "S_qRlL", "V_qLlL", "V_qRlL", "T_qLlL", "S_qLlR", "S_qRlR", "V_qLlR", "V_qRlR", "T_qRlR"
    // pHam->setWilsonCoefficientsLocal("BtoCTauNu", {1.,  val * 1i, 0., 0., 0., val / 4., 0., 0., 0., 0., 0.});
    pHam->setWilsonCoefficientsLocal("BtoCMuNu", {{"V_qRlL", val}});
    pHam->setFFEigenvectorsLocal("BD*", "CLNVar", {{"delta_R1",val}, {"delta_R2",val/3.}});
    // Note this is an incremental setting; only changes what you specify! One could do
    // ham.resetWilsonCoefficients("BtoCTauNu"); to reset to SM
    // One could also do e.g. ham.setWilsonCoefficients("BtoCMuNu", ...); if one cared about muon mode NP, etc.
    // The output is row major flattened vector, so you need to know the dims
    auto histoDs = pHam->getHistogram(histoname, "Scheme1");

    (*results)[id] = calcHisto(histoDs);
  };
    cout << "Starting serial evaluation of " << NUM << " evaluations of a 4D histogram " << endl;
    auto rwgtstart = std::chrono::system_clock::now();
    for (size_t idW = 0; idW < NUM; ++idW) {
      fun(idW, &ham, &serialResults);
    }
    auto rwgtend = std::chrono::system_clock::now();
    auto durrwgt = rwgtend - rwgtstart;
    auto secsrwgt = std::chrono::duration_cast<float_seconds>(durrwgt);
    cout << "Serial Reweight Time: " << secsrwgt.count() << endl;

  size_t tmp = std::thread::hardware_concurrency();
  cout << "number of threads " << tmp << endl;
  for (size_t n = 2; n <= 8; n *= 2) {
  size_t numThreads = tmp == 0 ? n : std::min(n, tmp);
        cout << "Starting parallel evaluation of " << NUM
             << " evaluations of 4D histogram  with " << numThreads
             << " threads:" << endl;
        auto rwgtstart = std::chrono::system_clock::now();
        parallelFor(numThreads, 0, NUM, [&](size_t start, size_t end) {
            for (size_t idW = start; idW < end; ++idW) {
	      fun(idW, &ham, &parallelResults);
	    }
        });

        auto rwgtend = std::chrono::system_clock::now();
        auto durrwgt = rwgtend - rwgtstart;
        auto secsrwgt = std::chrono::duration_cast<float_seconds>(durrwgt);
        cout << "Parallel Reweight Time with " << numThreads << " threads: " << secsrwgt.count() << endl;
	}

    cout << "Now comparing parallel vs serial results: ";
    double avgDiscrepancy = 0.;
    
    for (size_t i = 0; i < NUM; ++i) {
     
        cout << "parallel results = " << parallelResults[i] << endl;
        cout << "serial results = " << serialResults[i] << endl;
        double minval = min(serialResults[i], parallelResults[i]);
        double maxval = max(serialResults[i], parallelResults[i]);
        double term = fabs(serialResults[i]-parallelResults[i]);
        if(minval > 0.) {
            term /= (minval * NUM);
        }
        else if(maxval > 0.) {
            term /= (maxval * NUM);
        }
        else {
            term = 0.;
        }
        avgDiscrepancy += term;
    }
    cout << "average discrepancy = " << avgDiscrepancy << endl;

  } 
